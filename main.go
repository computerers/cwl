package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/fatih/color"
)

func main() {
	command := parseCommand()

	sess, err := session.NewSessionWithOptions(
		session.Options{SharedConfigState: session.SharedConfigEnable},
	)
	if err != nil {
		fmt.Println(err.Error())
	}
	svc := cloudwatchlogs.New(sess)

	nextToken, count := readAndPrintLogItems(svc, command, nil, 0)
	for nextToken != nil && count < command.limit {
		nextToken, count = readAndPrintLogItems(svc, command, nextToken, count)
	}
}

func readAndPrintLogItems(svc *cloudwatchlogs.CloudWatchLogs, comm *command,
	nextToken *string, count int64) (*string, int64) {
	interleaved := true

	var requestLimit int64
	requestLimit = 10000
	if comm.limit != 0 && comm.limit <= 1000 {
		requestLimit = comm.limit
	}

	params := &cloudwatchlogs.FilterLogEventsInput{
		LogGroupName:  aws.String(comm.logGroupName),
		FilterPattern: aws.String(comm.filter),
		Interleaved:   &interleaved,
		Limit:         aws.Int64(requestLimit),
		NextToken:     nextToken,
		StartTime:     aws.Int64(comm.start.UTC().Unix() * 1000),
		EndTime:       aws.Int64(comm.end.UTC().Unix() * 1000),
	}

	if len(comm.streams) > 0 {
		var streamPtrs []*string
		for _, stream := range comm.streams {
			streamPtrs = append(streamPtrs, &stream)
		}
		params.LogStreamNames = streamPtrs
	}

	resp, err := svc.FilterLogEvents(params)

	if err != nil {
		// Print the error, cast err to awserr.Error to get the Code and
		// Message from an error.
		fmt.Println(err.Error())
		return nil, count
	}

	for _, event := range resp.Events {
		printLogItem(comm, event)
		count += 1

		if comm.limit != 0 && count >= comm.limit {
			break
		}
	}

	return resp.NextToken, count
}

var (
	colors       = make(map[string]color.Attribute)
	colorOptions = []color.Attribute{
		color.FgRed,
		color.FgGreen,
		color.FgYellow,
		color.FgBlue,
		color.FgMagenta,
		color.FgCyan,
	}
)

func jsonObject(message string) map[string]interface{} {
	var j map[string]interface{}
	err := json.Unmarshal([]byte(message), &j)

	if err != nil {
		return nil
	}
	return j
}

func printLogItem(command *command, event *cloudwatchlogs.FilteredLogEvent) {
	var json map[string]interface{}

	if command.onlyJSON {
		json = jsonObject(*event.Message)

		if json == nil {
			return
		}
	}

	colorAttr, ok := colors[*event.LogStreamName]
	if !ok {
		colorAttr = colorOptions[len(colors)%len(colorOptions)]
		colors[*event.LogStreamName] = colorAttr
	}

	c := color.New(colorAttr)

	var stream string
	if command.display == "abbreviated-stream" {
		stream = (*event.LogStreamName)[0:10]
	} else if command.display == "full-stream" {
		stream = (*event.LogStreamName)
	}

	if command.ingestion {
		ingestionTime := time.Unix(*event.IngestionTime/1000, *event.IngestionTime%1000*1000)
		c.Printf("%s ", ingestionTime.Format("2006-01-02 15:04:05-0700"))
	}
	if command.display != "no-stream" {
		c.Print(string(stream))
		c.Print("|")
	}

	msg := *event.Message
	if len(command.selectFields) > 0 {
		msg = filterMessage(command, json)
	}
	fmt.Printf("%v\n", msg)
}

func filterMessage(command *command, eventData map[string]interface{}) string {
	filtered := make(map[string]interface{})

	for _, field := range command.selectFields {
		filtered[field] = eventData[field]
	}

	out, err := json.Marshal(filtered)
	if err != nil {
		panic(err)
	}
	return string(out)
}
